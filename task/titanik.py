import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(r' ([A-Za-z]+\.)')
    titles = ["Mr.", "Mrs.", "Miss."]
    result = []
    for title in titles:
        title_df = df[df['Title'] == title]
        title_df = title_df.dropna(subset=['Age'])
        if not title_df.empty:
            age_median = round(title_df['Age'].median())
        else:
            age_median = None
        missing_values = len(title_df)
        result.append((title, missing_values, age_median))
    return result
